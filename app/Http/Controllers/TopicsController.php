<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateTopicRequest;
use App\Post;
use App\Topic;
use App\Transformers\TopicTransformer;
use App\Http\Requests\StoreTopicRequest;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TopicsController extends Controller
{
    public function index()
    {
         $topics = Topic::latestFirst()->paginate(2);

         return fractal()
             ->collection($topics->getCollection())
             ->transformWith(new TopicTransformer())
             ->parseIncludes(['user'])
             ->paginateWith(new IlluminatePaginatorAdapter($topics))
             ->toArray();
    }

    public function show(Topic $topic)
    {
        return fractal()
            ->item($topic)
            ->transformWith(new TopicTransformer)
            ->parseIncludes(['user', 'posts', 'posts.user', 'posts.likes'])
            ->toArray();
    }

    public function store(StoreTopicRequest $request)
    {
        $topic = new Topic;
        $topic->title = $request->title;
        $topic->user()->associate($request->user());

        $post = new Post;
        $post->body = $request->body;
        $post->user()->associate($request->user());

        $topic->save();
        $topic->posts()->save($post);

        return fractal()
            ->item($topic)
            ->transformWith(new TopicTransformer)
            ->parseIncludes(['user'])
            ->toArray();
    }

    public function update(UpdateTopicRequest $request, Topic $topic)
    {
        // call TopicPolicy.php @ userIsOwner function
        $this->authorize('userIsOwner', $topic);

        $topic->title = $request->get('title', $topic->title);
        $topic->save();

        return fractal()
            ->item($topic)
            ->parseIncludes(['user'])
            ->transformWith(new TopicTransformer)
            ->toArray();
    }

    public function destroy(Topic $topic)
    {
        $this->authorize('userIsOwner', $topic);

        $topic->delete();

        return response(null, 204);
    }
}
