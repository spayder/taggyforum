<?php

namespace App\Http\Controllers;

use App\Post;
use App\Topic;
use App\Transformers\PostTransformer;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

class PostsController extends Controller
{
    public function store(StorePostRequest $request, Topic $topic)
    {
        $post = new Post;
        $post->body = $request->body;
        $post->user()->associate($request->user());

        $topic->posts()->save($post);

        return fractal()
            ->item($post)
            ->transformWith(new PostTransformer)
            ->parseIncludes(['user'])
            ->toArray();
    }

    public function update(UpdatePostRequest $request,Topic $topic, Post $post)
    {
        $this->authorize('userIsOwner', $post);
        $post->body = $request->get('body', $post->body);
        $post->save();

        return fractal()
            ->item($post)
            ->transformWith(new PostTransformer)
            ->parseIncludes(['user'])
            ->toArray();
    }

    public function destroy(Topic $topic, Post $post)
    {
        $this->authorize('userIsOwner', $post);

        $post->delete();

        return response(null, 204);
    }
}
