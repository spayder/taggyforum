<?php

namespace App\Policies;

use App\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function userIsOwner(User $user, Post $post)
    {
        return $user->ownsPost($post);
    }

    public function userIsNotOwner(User $user, Post $post)
    {
        return ! $user->ownsPost($post);
    }
}
