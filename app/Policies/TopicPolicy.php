<?php

namespace App\Policies;

use App\Topic;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TopicPolicy
{
    use HandlesAuthorization;

    public function userIsOwner(User $user, Topic $topic)
    {
        return $user->ownsTopic($topic);
    }
}
