<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('/register', 'RegisterController@register');

Route::group(['prefix' => 'topics'], function() {
    Route::get('/', 'TopicsController@index');
    Route::get('/{topic}', 'TopicsController@show');
    Route::post('/', 'TopicsController@store')->middleware('auth:api');
    Route::patch('/{topic}', 'TopicsController@update')->middleware('auth:api');
    Route::delete('/{topic}', 'TopicsController@destroy')->middleware('auth:api');

    Route::group(['prefix' => '/{topic}/posts', 'middleware' => 'auth:api'], function() {
       Route::post('/', 'PostsController@store');
       Route::patch('/{post}', 'PostsController@update');
       Route::delete('/{post}', 'PostsController@destroy');

       Route::group(['prefix' => '/{post}/likes'], function() {
          Route::post('/', 'PostsLikeController@store');
       });
    });
});